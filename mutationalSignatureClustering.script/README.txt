########################################################
### Author: Maria Secrier (maria.secrier@cantab.net)
### The scripts included in this folder are free to use, 
### modify and distribute, as long as the source is 
### acknowledged should they be used in a publication.
###
### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
### OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
### IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
### OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE 
### USE OR OTHER DEALINGS IN THE SOFTWARE. 

Structure of the folder:
> plotMutationalSignatures.R
Script used to cluster and plot the samples in a cohort according to their mutational signature exposures (obtained from the NMF methodology or other methods).

> sampleData/
Contains input files needed to run the analysis (mainly information about signature contributions).

> output/
Contains the plot of samples clustered by mutational signatures.
