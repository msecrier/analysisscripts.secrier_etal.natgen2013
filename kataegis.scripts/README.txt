########################################################
### Author: Maria Secrier (maria.secrier@cantab.net)
### The scripts included in this folder are free to use, 
### modify and distribute, as long as the source is 
### acknowledged should they be used in a publication.
###
### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
### OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
### IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
### OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE 
### USE OR OTHER DEALINGS IN THE SOFTWARE. 

Structure of the folder:
> kataegisInference.R
Script used to infer kataegis regions as described in the paper.

> sampleData/
Contains sample SNV data needed for running the script, as well as related files (file containing chromosome lengths). 

> rainfallPlots/
Contains the rainfall plots generated for each sample.

> output/
Contains the inferred kataegis regions
