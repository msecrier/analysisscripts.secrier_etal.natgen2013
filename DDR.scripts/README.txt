########################################################
### Author: Maria Secrier (maria.secrier@cantab.net)
### The scripts included in this folder are free to use, 
### modify and distribute, as long as the source is 
### acknowledged should they be used in a publication.
###
### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
### OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
### IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
### OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE 
### USE OR OTHER DEALINGS IN THE SOFTWARE. 

Structure of the folder:
> significantlyMutatedGenes.R
Script used to infer genes mutated above background as described in the paper and Puente et al.
Should be run first.

> DDRanalysis
Script used to infer frequencies of mutated DDR pathways and plot the results.
Should be run after the inference of significantly mutated genes.

> sampleData/
Contains sample SNV data needed for running the script, as well as related files:
- files containing details of chromosome lengths, all genes in the hg19 from Ensembl; 
- DDRgenes.txt - all genes involved in DDR; 
- DDRpathways.xlsx - details of all DDR-related genes and pathways involved 

> output/
Contains the inferred significantly mutated genes. The file probNonsyn.genes.adjusted.significant.txt should be used for downstream analysis.

> plots/
Contains the radial plots used to illustrate frequency of mutated DDR pathways in the cohort.
