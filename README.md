# README #

### Contents ###

* This folder contains various scripts used for the analysis in the following paper:
http://www.nature.com/ng/journal/v48/n10/full/ng.3659.html

* Includes kataegis, chromothripsis, BFB identification; DDR and neoantigen analysis; mutational signature clustering.