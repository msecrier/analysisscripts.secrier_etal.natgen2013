#################################################################################################################
#*** Author: Maria Secrier (maria.secrier@cantab.net)
#*** Last modified: 28/06/2016
#*** Description: This script combines the results of all matrices to infer chromothripsis events.
#################################################################################################################

##################
#### Libraries
##################

library(GenomicRanges)
library(gplots)
library(RColorBrewer)

#####################
#### Data processing
#####################

### Read in scoring matrices:
matBP <- read.table("scoringMatrices/breakpointClustering.matrix.txt", header=TRUE, sep="\t",
                    stringsAsFactors=FALSE)
matLOH <- read.table("scoringMatrices/interspersedLOH.matrix.txt", header=TRUE, sep="\t",
                    stringsAsFactors=FALSE)
matCN <- read.table("scoringMatrices/regularCNstates.matrix.txt", header=TRUE, sep="\t",
                     stringsAsFactors=FALSE)
matRandJoin <- read.table("scoringMatrices/randDNAfragmentJoins.matrix.txt", header=TRUE, sep="\t",
                    stringsAsFactors=FALSE)
matRandOrder <- read.table("scoringMatrices/randDNAfragmentOrder.matrix.txt", header=TRUE, sep="\t",
                          stringsAsFactors=FALSE)
matWalkDerivChr <- read.table("scoringMatrices/walkDerivChr.matrix.txt", header=TRUE, sep="\t",
                           stringsAsFactors=FALSE)

### Read in copy number data:
load("../sampleData/copyNumberCalls.sampleCohort.RData")

##################
#### Analysis
##################

#### Matrix that stores the final scores for each patient:
patientScores <- array(0,dim(matBP))
colnames(patientScores) <- colnames(matBP)
rownames(patientScores) <- rownames(matBP)

### For each patient, count the number of fulfilled criteria:
# NB: the cut-offs for the various criteria vary and can be set according to preferences
k=0
numPatientsWithChromothripsis <- 0 # number of patients with chromothripsis
for (patient in unique(rownames(matBP))) {
   for (chr in 1:24) {
    cn.current <- cndata[which((cndata$Illumina_Barcode_full == patient) &
                           (cndata$Chromosome == chr)),]
    if (nrow(cn.current)>10) { #criterion of more than 10 switches
      if (matBP[which(rownames(matBP)==patient),chr]<0.0001) {
        # (this is the most important criterion, so it could be scored higher, e.g. +3)
        patientScores[patient,chr] <- patientScores[patient,chr]+1
      }
      if (length(matLOH[which(rownames(matLOH)==patient),chr]>0)) {
        if (matLOH[which(rownames(matLOH)==patient),chr]>=0.50) {
          # (this is an important criterion, so I give it could be scored higher, e.g. +2)
          patientScores[patient,chr] <- patientScores[patient,chr]+1
        }
      }
      if (length(matCN[which(rownames(matCN)==patient),chr]>0)) {
        if (matCN[which(rownames(matCN)==patient),chr]>=0.50) {
          # (this is an important criterion, so it could be scored higher)
          patientScores[patient,chr] <- patientScores[patient,chr]+1
        }
      }
      if (matRandJoin[which(rownames(matRandJoin)==patient),chr]<0.001) {
        # less important as criterion, 1 point:
        patientScores[patient,chr] <- patientScores[patient,chr]+1
      }
      if (matRandOrder[which(rownames(matRandOrder)==patient),chr]<0.50) {
        # less important as criterion, 1 point:
        patientScores[patient,chr] <- patientScores[patient,chr]+1
      }
      if (matWalkDerivChr[which(rownames(matWalkDerivChr)==patient),chr]>0.05) {
        # less important as criterion, 1 point:
        patientScores[patient,chr] <- patientScores[patient,chr]+1
      }
    }
   }
   # If a chromosome scores high enough (at least 3), then it likely has chromothripsis:
   if (length(which(patientScores[patient,]>=3)>0)) {
     print(patient)
     numPatientsWithChromothripsis <- numPatientsWithChromothripsis+1
   }
}

### Plot a matrix of overall scores by chromosome for every sample:
# this is the color gradient for the heat map:
rdBuPalette <- colorRampPalette(rev(brewer.pal(11, "RdBu")))(n=7)
pdf("plots/chromothripsisScores.bySampleAndChr.pdf")
heatmap.2(as.matrix(patientScores), trace = "none", 
          scale = "none", 
          col = rdBuPalette,Rowv=TRUE, Colv=FALSE,    
          cexRow = 0.3, cexCol = 0.8)
dev.off()


# Save patient scores by chromosome:
write.table(patientScores,"chromothripsisScores.allPatients.txt",
            quote=FALSE, col.names=TRUE, row.names=TRUE)
