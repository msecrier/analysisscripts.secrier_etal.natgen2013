########################################################
### Author: Maria Secrier (maria.secrier@cantab.net)
### The scripts included in this folder are free to use, 
### modify and distribute, as long as the source is 
### acknowledged should they be used in a publication.
###
### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
### OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
### IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
### OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE 
### USE OR OTHER DEALINGS IN THE SOFTWARE. 

Structure of the folder:
> chromothripsisStatisticalCriteriaScripts/
Contains all scripts and associated outputs created to assess 6/7 of the chromothripsis criteria in the Korbel&Campbell paper, Cell 2013. The cut-offs used are variable and could benefit from fine-tuning. The various criteria are individually assessed in appropriately named files (6 in total), and the script combineMatForChromothripsis.R puts together all the scores from the individual criteria to produce an overall score for each patient and chromosome. The individual criteria scores are stored in scoringMatrices/ and the associated plots in the plots/ folder. The final scores by chromosome for each patient are saved in chromothripsisScores.allPatients.txt.
NB: these automatically generated scores often don't reflect what one would expect by visual inspection, and therefore should be taken for informative purposes only - rather than for an ultimate decision on chromothripsis.

> chromothripsisVisualizationScripts/
Contains the script required to plot SV and copy number data by chromosome in a way that makes visual assessment of chromothripsis easy. Visual inspection of the resulting plots is the main method used in our work to assess chromothripsis.

> sampleData/
Contains sample SV and copy number data needed for running the scripts, as well as related files (file containing chromosome lengths). 

