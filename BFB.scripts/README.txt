################################################################################
### Author: Tsun-Po Yang (tsun-po.yang@uni-koeln.de), Maria Secrier (maria.secrier@cantab.net)
### The scripts included in this folder are free to use, 
### modify and distribute, as long as the source is 
### acknowledged should they be used in a publication.
###
### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
### OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
### IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
### OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE 
### USE OR OTHER DEALINGS IN THE SOFTWARE. 

Structure of the folder:
> sv_inv_bfb_naive.R
Script used to infer BFB regions as described in the paper.

> sampleData/
Contains sample SV and copy number data (from ASCAT) needed for running the script.

> plots/
Contains the SV plots generated for each sample.

> output/
Contains various intermediate results file and the final file that scores the clustered inversion regions according to their likelihood of being BFB events.
Files to check:
finalReport_enrichmentOfInversions.txt - contains scores for BFB loci by chromosome (enrichment of clustered inversions in the region - the higher the more likely BFB events, ideally prioritise scores>1)
finalReport_potentialBFBloci.txt - summarised potential BFB loci across the cohort

NB: these scripts only produce rough statistics of inversion clustering and plot inversions per chromosome for each sample. The final decision for BFB regions is performed  by manual curation using the criteria described in the paper.

Please redirect any questions to Tsun-Po Yang (tsun-po.yang@uni-koeln.de).

