#!/usr/bin/bash

#### This script runs the analysis on all HLAs and all samples available
#### in samples.txt

while read sample;
    do
    bsub -q stlab-icgc -R "rusage[mem=8000]" -o logs/$sample.WT.log -e logs/$sample.WT.err -J "${sample}wt[1-67]%2" "sh run.netmhc.allhla.sh $sample WT"
    bsub -q stlab-icgc -R "rusage[mem=8000]" -o logs/$sample.MUT.log -e logs/$sample.MUT.err -J "${sample}mut[1-67]%2" "sh run.netmhc.allhla.sh $sample MUT"
done < samples.txt
