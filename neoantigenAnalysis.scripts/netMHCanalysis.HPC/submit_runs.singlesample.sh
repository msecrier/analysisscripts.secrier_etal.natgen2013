#!/usr/bin/bash

### This script submits a run on the HPC for a specified sample and HLA

sample=$1
hla=$2
  bsub -q stlab-icgc -R "rusage[mem=8000]" -o logs/single.$sample.WT.log -e logs/single.$sample.WT.err -J ${sample}wt "sh run.netmhc.sh $sample WT $hla"
  bsub -q stlab-icgc -R "rusage[mem=8000]" -o logs/single.$sample.MUT.log -e logs/single.$sample.MUT.err -J ${sample}mut "sh run.netmhc.sh $sample MUT $hla"
