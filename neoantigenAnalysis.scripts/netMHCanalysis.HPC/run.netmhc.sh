### This script runs the netMHC software on the selected sample and HLA
## the condition refers to either the WT or mutant peptide

# Illumina barcode for the sample I want to run:
sample=$1
# either WT or MUT - to look at affinity or binding of WT or mutant peptides:
condition=$2
# the HLA I want to run the algorithm against: 
hla=$3
../netMHC-3.4/netMHC -a $hla input/$condition/$sample.17AA_$condition.txt > predictions/$condition/$sample.predicted.${hla}_${condition}.txt

# example run: sh run.netmhc.sh LP6005334-DNA_A02 MUT HLA-A01:01

#example HLAs: HLA-A03:01,HLA-A32:01,HLA-B08:01,HLA-B51:01,HLA-C07:02,HLA-C15:02 
