###############
#### Author: Maria Secrier (maria.secrier@cantab.net)
#### This workflow is provided as is and is free to modify, redistribute
#### and used for publication as long as the source is acknowleged.

To run the netMHC software on all samples (provided suitable input is provided):
sh submit_runs.allhla.sh

(NB: netMHC needs to be installed separately!)

This script calls the run.netmhc.allhla.sh, which runs the tool on a sample-by-sample basis. The hlaTypes.txt file contains all HLA alleles that we wish to survey and the samples.txt file contains the IDs of all samples we wish to analyse. The input for netMHC should be stored in input/ (the WT and MUT peptides should be given for each sample in separate folders) - input can be obtained from prepareInputForNetMHC/; and similarly the output of binding predictions will be save in the predictions/ folder (separately for WT and MUT peptides) for each sample. This output will then need to be merged for the neoantigen quantification analysis in processNeoantigens/.

PLEASE CONSULT THE NETMHC DOCUMENTATION FOR THE PROPER SETUP AND UNDERSTANDING OF THE ANALYSIS INVOLVED. NETMHC WILL NEED TO BE INSTALLED SEPARATELY, IT IS NOT PROVIDED WITH THIS CODE.
