### This script runs the netMHC software on all HLAs in the hlaTypes.txt file

# The sample to analyse:
sample=$1
# Condition (WT/MUT):
condition=$2

IFS=$'\n' read -d '' -r -a hlaALL < hlaTypes.txt

hla=${hlaALL[$LSB_JOBINDEX]}
../netMHC-3.4/netMHC -a $hla input.restNaive/$condition/$sample.17AA_$condition.txt > predictions.last5/$condition/$sample.predicted.${hla}_${condition}.txt

