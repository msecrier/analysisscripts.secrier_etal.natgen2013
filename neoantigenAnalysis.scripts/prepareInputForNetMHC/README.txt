###############
#### Author: Maria Secrier (maria.secrier@cantab.net)
#### This workflow is provided as is and is free to modify, redistribute
#### and used for publication as long as the source is acknowleged.

This folder contains the analysis required to prepare the input files for netMHC. From SNV call files, we infer which mutations will cause a change in the amino acid of the protein and extract 17amino acid peptides centered on the mutated amino acid. The respective WT peptides (before mutation) are also saved separately. The peptide sequences are prepared for netMHC analysis.

sampleInput/ : contains an example file with SNV calls for 5 samples
humanProteome.fasta.txt: the human proteome sequences extracted in fasta format from Uniprot
input.forNetMHC/ : sample output folder, will contain the input for netMHC analysis (importantly, there are 2 separate folders for the WT and MUT peptides).

snvs_extract17AAsequence.RELEASED.R: script that extracts the peptide information for netMHC; instructions on how to run it can be found inside


