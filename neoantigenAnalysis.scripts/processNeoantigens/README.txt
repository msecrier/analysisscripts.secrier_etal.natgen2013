###############
#### Author: Maria Secrier (maria.secrier@cantab.net)
#### This workflow is provided as is and is free to modify, redistribute
#### and used for publication as long as the source is acknowleged.

This folder contains the analysis required to process the output from netMHC and quantify the neoantigen load in cancer samples. The peptides that have high or at least weak affinity to the HLA molecules are selected. The affinity is quantified both for the mutated and the normal peptide and only those peptides with affinity in the tumour but none in the normal are selected for analysis.

sampleInput/ : contains an example file with binding affinity output for peptides from netMHC (for 5 samples as example - the data were merged together a priori)
output/ : sample output folder, contains some plots that enable the visualisation of neoantigen burden in multiple samples

processNeoantigens.RELEASED.R : script that performs the neoantigen quantification by sample; instructions on how to run it can be found inside


