##############################################################################
### Author: Maria Secrier (maria.secrier@cantab.net)
###
### The scripts included in this folder and all its subfolders
### are free to use, modify and distribute, as long as the source is 
### acknowledged should they be used in a publication.
###
### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
### OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
### IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
### OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE 
### USE OR OTHER DEALINGS IN THE SOFTWARE. 

Structure of the folder (analysis to be run in this order):

prepareInputForNetMHC/ : contains scripts required to extract mutated peptides (from SNV calls on a cohort of samples) to be used subsequently as input for netMHC analysis

netMHCanalysis.HPC/ : contains scripts required to run netMHC on an HPC cluster (NB: commands will vary depending on the type of HPC; please consult the netMHC documentation for more details on the steps); example shown for a single sample, but the same setup would be used for a cohort of samples

processNeoantigens/: contains scripts required to process the netMHC output information and infer neoantigen loads in tumours




